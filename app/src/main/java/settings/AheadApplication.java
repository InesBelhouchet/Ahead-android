package settings;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by Ines Belhouchet on 2/17/17
 * project Name: Ahead
 */

public class AheadApplication extends Application {

  @Override public void onCreate() {
    super.onCreate();
    Realm.init(this);

  }
}
