package authenticationManager;

import android.util.Log;

import io.realm.Realm;
import io.realm.RealmResults;

import models.NewUserModel;
import storage.SaveNewUser;

/**
 * Authentication manager
 * Created by Ines Belhouchet on 2/14/17
 * project Name: Ahead
 */

public class LoginManager {

  /**
   * reserved to signUP activity
   * check user : add it if it's a new user.
   *
   * @param newUserModel a UserModel
   * @return result of user state
   */
  public int checkuserInput(SaveNewUser newUserModel) {
    if (testEmailInput(newUserModel.getEmailUser())) {
      if (testPasswordInput(newUserModel.getPasswordUser())) {
        if (testUserNameInput(newUserModel.getUserNameUser())) {
          return checkExist("signUP", newUserModel);
        } else {
          return 4;
        }
      } else {
        return 3;
      }
    } else {
      return 2;
    }
  }



  /**
   * reserved to signIN activity
   * check user login
   *
   * @param newUserModel a UserModel
   * @return result of user state
   */
  public int checkLoginUser(SaveNewUser newUserModel) {
    if (testEmailInput(newUserModel.getEmailUser())) {
      if (testPasswordInput(newUserModel.getPasswordUser())) {
        return checkExist(null, newUserModel);
      }else {
          return 4;
        }
      } else {
        return 3;
      }
    }




  /**
   * email data verification
   *
   * @param emailInput email of the user
   * @return result of verification
   */
  private Boolean testEmailInput(String emailInput) {
      return emailInput.length() > 0;
  }

  /**
   * userName data verification
   *
   * @param userNameInput userName of the user
   * @return result of verification
   */
  private Boolean testUserNameInput(String userNameInput) {
    return userNameInput.length() > 0;
  }

  /**
   * password data verification
   *
   * @param passwordInput user's password
   * @return result of verification
   */
  private Boolean testPasswordInput(String passwordInput) {
    return passwordInput.length() > 0;
  }

  /**
   * check if the user instance exists already on  database
   *
   * @param newUserModel a model User
   * @return result of realm verification, 0: it's a new user , add it to the DB
   * if it's 1 then turn back to the signIn activity
   */
  private int checkExist(String tagLogin, SaveNewUser newUserModel) {
    //@TODO-check if the user exist in server

    //1-check if the user exists in realm
    Log.d("loguser", " " + newUserModel.getEmailUser());

    // Get a Realm instance for this thread
    Realm realm = Realm.getDefaultInstance();

   // final RealmResults<SaveNewUser> users =
     //   realm.where(SaveNewUser.class).equalTo("emailUser", newUserModel.getEmailUser()).findAll();


    //final RealmResults<SaveNewUser> users =
   //     realm.where(SaveNewUser.class).findAll();

      final RealmResults<SaveNewUser> users =
      realm.where(SaveNewUser.class).equalTo("emailUser", newUserModel.getEmailUser()).
           equalTo("passwordUser", newUserModel.getPasswordUser()).findAll();



    Log.d("RealmResults", users.contains(newUserModel) + " ");
    if (users.size() != 0) {

      return 1;
    } else {
      if (tagLogin == "signUP"){
      realm.beginTransaction();
      final SaveNewUser newAccount = realm.createObject(SaveNewUser.class);
      newAccount.setUserNameUser(newUserModel.getUserNameUser());
      newAccount.setEmailUser(newUserModel.getEmailUser());
      newAccount.setPasswordUser(newUserModel.getPasswordUser());
      realm.commitTransaction();

      return 0;
    }
      return -1;
    }

  }

  /**
   * fetch this email address into server
   * send the found password to the user email box
   *
   * */
  public void forgetPassword(String userEmail) {
    //Retrofit call
  }
}
