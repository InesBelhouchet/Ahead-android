package activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Explode;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import authenticationManager.LoginManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import programming.inesso.hardwork.ahead.R;
import storage.SaveNewUser;

public class SignINActivity extends Activity {

  private LoginManager loginManager;
  private Context context;

  @BindView(R.id.editText_email) EditText emailEditText;
  @BindView(R.id.editText_forgetAccount) EditText forgetPasswordEditText;
  @BindView(R.id.editText_password) EditText passwordEditText;
    @BindView(R.id.button_forget_password) Button forgetPassworButton;
    @BindView(R.id.button_signin) Button buttonSignIn;
  @BindView(R.id.button_gotosignup) Button buttonSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        setContentView(R.layout.signin);
        ButterKnife.bind(this);
      context = this.getApplicationContext();
      loginManager = new LoginManager();
    }
  @OnClick(R.id.button_forget_password) public void forgetPassword() {
    //showup a messageBox for inserting email
    forgetPasswordEditText.setVisibility(View.VISIBLE);
    forgetPassworButton.setVisibility(View.GONE);
    //connect to server
    loginManager.forgetPassword(forgetPasswordEditText.getText().toString());
  }
  @OnClick(R.id.button_signin) public void signInButton(){
      int loginResult = loginManager.checkLoginUser(addNewUserModel());
    handleLoginResult(loginResult);
  }

  /**
   * display result of login process .
   * @param loginResult loginResult
   */
  private void handleLoginResult(int loginResult) {
    switch (loginResult){
      case -1:
        Toast.makeText(context, "you have to add an account, sign UP please", Toast.LENGTH_LONG).
            show();
        goTOsignUp();
        break;
      case 1:
        Toast.makeText(context, "Welcome to Ahead", Toast.LENGTH_LONG).show();
        break;
      case 3:
        Toast.makeText(context, "Add your email address please", Toast.LENGTH_LONG).show();
        break;
      case 4:
        Toast.makeText(context, "check your password please", Toast.LENGTH_LONG).show();
        break;

    }
  }

  /**
   * create a user with mail, userName and password
   * @return a new user Data
   */
  public SaveNewUser addNewUserModel(){
    SaveNewUser newUser = new SaveNewUser();
    //NewUserModel newUser = new NewUserModel();
    newUser.setEmailUser(emailEditText.getText().toString());
    newUser.setPasswordUser(passwordEditText.getText().toString());
    return newUser;

  }

  @OnClick(R.id.button_gotosignup)public void goTOsignUp(){
    getWindow().setExitTransition(new Explode());
    Intent goBackTOSignUP = new Intent(context, SignUPActivity.class);
    startActivity(goBackTOSignUP, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
  }
}