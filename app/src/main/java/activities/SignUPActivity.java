package activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.EditText;

import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import authenticationManager.LoginManager;
import programming.inesso.hardwork.ahead.R;
import storage.SaveNewUser;

/**
 *
 * signIn a user with email and password:
 * @TODO 1-offline mode(with realm database support )
 * @TODO 2-connection to server
 *
 * Created by Ines Belhouchet on 2/14/17
 * project Name: Ahead
 *
 */
public class SignUPActivity extends Activity{

  @BindView(R.id.editText_username_up) EditText userNameEditText;
  @BindView(R.id.editText_email_up) EditText emailEditText;
  @BindView(R.id.editText_password_up) EditText passwordEditText;
  @BindView(R.id.button_signup) Button buttonSignUP;
  private LoginManager loginManager;
  private Context context;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.signup);
    ButterKnife.bind(this);
    context = this.getApplicationContext();
    loginManager = new LoginManager();
  }
    @OnClick(R.id.button_signup) public void addNewUser() {
        //check input user
      int resultBerficationUser = loginManager.checkuserInput(addNewUserModel());
      if(resultBerficationUser == 1){
        Toast.makeText(context, context.getResources().getString(R.string.cantsignup),
            Toast.LENGTH_LONG).
            show();

        Intent goBackTOSignIN = new Intent(context, SignINActivity.class);
        startActivity(goBackTOSignIN);
      }else{
        showMassageResult(resultBerficationUser);
      }

    }

  /**
   * According to result a toast is performed to diplay the required message
   * @param result result of login process
   */
  private void showMassageResult(int result) {

    switch (result){
      case 0:
        Toast.makeText(context, "Welcome to Ahead",
            Toast.LENGTH_LONG).
            show();
        break;
      case 2:
        Toast.makeText(context, "Add your email address please",
            Toast.LENGTH_LONG).
            show();
        break;
      case 3:
        Toast.makeText(context, "check your password please",
            Toast.LENGTH_LONG).
            show();
        break;
      case 4:
        Toast.makeText(context, "check your user name please",
            Toast.LENGTH_LONG).
            show();
        break;
    }
  }

  /**
   * create a user with mail, userName and password
   * @return a new user Data
   */
  public SaveNewUser addNewUserModel(){
    SaveNewUser newUser = new SaveNewUser();
    newUser.setEmailUser(emailEditText.getText().toString());
    newUser.setPasswordUser(passwordEditText.getText().toString());
    newUser.setUserNameUser(userNameEditText.getText().toString());
    return newUser;

  }
}
