package models;

import io.realm.RealmModel;

/**
 * a user model
 *
 * Created by Ines Belhouchet on 2/14/17
 * project Name: Ahead
 */

public class NewUserModel {

  private String emailUser;
  private String passwordUser;
  private String userNameUser;

  public String getEmailUser() {
    return emailUser;
  }

  public void setEmailUser(String emailUser) {
    this.emailUser = emailUser;
  }

  public String getPasswordUser() {
    return passwordUser;
  }

  public void setPasswordUser(String passwordUser) {
    this.passwordUser = passwordUser;
  }

  public String getUserNameUser() {
    return userNameUser;
  }

  public void setUserNameUser(String userNameUser) {
    this.userNameUser = userNameUser;
  }

}
